import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.linear_model import LinearRegression
from scipy import stats # For in-built method to get PCC


def organize(df,plot=False,resample='60min'):
    

    df['end'] = pd.to_datetime(df['end'].astype("datetime64[ms]"))

    df.index = pd.to_datetime(df.end, unit='ms')
    df.set_index(df.end)

    dff = df[df['rand'] == True]
    dft = df[df['rand'] == False]

    del dft['rand']
    del dft['end']
    
    del dff['rand']
    del dff['end']    
    
    if plot == True:
        tt = dft.resample(resample).sum()
        tf = dff.resample(resample).sum()
        
        ax1 = tt.plot(color='blue',figsize=(55,25))
        _   = tf.plot(ax=ax1)
    
    return df, dft, dff


def reg_rt(X, y,ax=None):

    
    data = {'y':y, 'X':X}
    data = pd.DataFrame.from_dict(data)
    
    
    # Linear Regression
    reg = LinearRegression().fit(X.reshape(-1, 1), y.reshape(-1, 1))
    reg.score(y.reshape(-1, 1), X.reshape(-1, 1))
    print('intercept: {:.2f}  coef {:.2f}'.format( float(reg.intercept_[0]) , float(reg.coef_[0])))


    ax = sns.regplot(x="X", y="y", data=data)
#    plt.figure()
#    sns.jointplot(x="X", y="y", data=data);


    pearson_coef, p_value = stats.pearsonr(data.y, data.X) #define the columns to perform calculations on
    print("Pearson Correlation Coefficient: {:.4f}".format(pearson_coef), "and a P-value of: {}".format( p_value)) # Results 

    
    
def print_freq(mac_true,mac_random):
    
    print('---------------------------')
    print('Conteggi e frequenze')
    print('sum random: {:.2f}'.format( np.sum(mac_random)),'sum non random: {:.2f}'.format( np.sum(mac_true)))
    print('freq h random: {:.2f}'.format( np.sum(mac_random)/(7*24)),'freq h non random: {:.2f}'.format( np.sum(mac_true)/(7*24)))
    print('freq min random: {:.2f}'.format( np.sum(mac_random)/(7*24*60)),'freq min non random: {:.2f}'.format( np.sum(mac_true)/(7*24*60)))
    print('---------------------------')